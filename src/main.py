import uvicorn
from fastapi import FastAPI, Response, Request, Form
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from schemas import TicketAddDTO, TicketDTO, TicketUpdateDTO
from typing import Optional
from fastapi.middleware.cors import CORSMiddleware
from orm import AsyncORM
from typing import List
import asyncio
import requests
from config import BOT_TOKEN


templates = Jinja2Templates(directory='templates')
send_message_url = f'https://api.telegram.org/bot{BOT_TOKEN}/sendMessage'

async def main():
    await AsyncORM.create_tables()


def create_fastapi_app():
    app = FastAPI(title="FastAPI")
    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
    )

    @app.get("/pages/tickets", response_class=HTMLResponse, tags=['pages'])
    async def show_tickets(request: Request):
        tickets = await AsyncORM.all_tickets()
        return templates.TemplateResponse("tickets.html", {"request": request, "tickets": tickets})

    @app.get("/tickets", tags=['tickets'], response_model=list[TicketDTO])
    async def show_tickets():
        res = await AsyncORM.all_tickets()
        return res

    @app.post("/status/ticket", tags=['status'])
    async def check_ticket_status(data: TicketAddDTO):
        chat_id = data.chat_id
        message = data.text
        res = await AsyncORM.check_ticket(chat_id=chat_id)
        if res is None:
            ticket = await AsyncORM.create_ticket(chat_id=chat_id, message=message)
            return ticket.id, 200
        else:
            return 'Тикет уже открыт', 400

    @app.patch("/tickets", tags=['status'])
    async def change_status(ticket_update: TicketUpdateDTO):
        ticket_id = ticket_update.ticket_id
        new_status = ticket_update.status
        ticket = await AsyncORM.new_status_ticket(ticket_id=ticket_id, new_status=new_status)
        payload = {'chat_id': ticket.chat_id, 'text': f'Статус тикетa изменен на {new_status}'}
        requests.post(send_message_url, data=payload)
        return f'status change to {new_status}'

    @app.patch("/answer", tags=['tickets'])
    async def answer_to_client(request: Request):
        data = await request.json()
        answer = data['answer']
        ticket_id = data['ticket_id']
        ticket = await AsyncORM.answer(answer=answer, ticket_id=ticket_id)
        payload = {'chat_id': ticket.chat_id, 'text': f'Ваш тикет обработан.\nОтвет оператора:\n{ticket.answer}'}
        requests.post(send_message_url, data=payload)
        return f'New answer: {ticket.answer}'

    @app.get("/sorted-creation-time", tags=['sorted'], response_model=List[TicketDTO])
    async def sort_created_time():
        res = await AsyncORM.sort_by_created_time()
        return res

    @app.get("/sorted-updation-time", tags=['sorted'], response_model=List[TicketDTO])
    async def sort_updated_time():
        res = await AsyncORM.sort_by_updated_time()
        return res

    @app.get("/sorted-by-status", tags=['sorted'], response_model=List[TicketDTO])
    async def sort_by_status():
        res = await AsyncORM.sort_by_status()
        return res

    return app


app = create_fastapi_app()


if __name__ == "__main__":
    asyncio.run(main())
    uvicorn.run(
        app="main:app",
        reload=True,
        host="0.0.0.0",
        port=8000,
    )
