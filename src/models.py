from typing import Annotated, Optional
from sqlalchemy.orm import Mapped, mapped_column
from database import Base, str_256
from sqlalchemy import text
import datetime


intpk = Annotated[int, mapped_column(primary_key=True)]
created_at = Annotated[datetime.datetime, mapped_column(server_default=text("TIMEZONE('utc', now())"))]
updated_at = Annotated[datetime.datetime, mapped_column(
        server_default=text("TIMEZONE('utc', now())"),
        onupdate=datetime.datetime.utcnow,
    )]


class TicketsORM(Base):
    __tablename__ = "tickets"

    id: Mapped[intpk]
    chat_id: Mapped[int]
    text: Mapped[str]
    status: Mapped[str] = mapped_column(default='open')
    answer: Mapped[Optional[str]] = mapped_column(default=None)
    created_at: Mapped[created_at]
    updated_at: Mapped[updated_at]
