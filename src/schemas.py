from pydantic import BaseModel
from datetime import datetime
from typing import Optional


class TicketUpdateDTO(BaseModel):
    ticket_id: int
    status: str


class TicketAddDTO(BaseModel):
    chat_id: int
    text: str


class TicketDTO(TicketAddDTO):
    id: int
    status: str
    answer: Optional[str]
    created_at: datetime
    updated_at: datetime
