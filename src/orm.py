from database import Base, async_engine, async_session_factory
from models import TicketsORM
from sqlalchemy import select, delete, and_, or_, case
from schemas import TicketDTO


class AsyncORM:
    @staticmethod
    async def create_tables():
        async with async_engine.begin() as conn:
            await conn.run_sync(Base.metadata.drop_all)
            await conn.run_sync(Base.metadata.create_all)

    @staticmethod
    async def create_ticket(chat_id, message):
        async with async_session_factory() as session:
            new_ticket = TicketsORM(chat_id=chat_id, text=message)
            session.add(new_ticket)
            await session.commit()
            await session.refresh(new_ticket)
            result_dto = TicketDTO.model_validate(new_ticket, from_attributes=True)
            return result_dto

    @staticmethod
    async def check_ticket(chat_id):
        async with async_session_factory() as session:
            query = (
                select(TicketsORM)
                .where(and_(TicketsORM.chat_id == chat_id,
                            or_(TicketsORM.status == 'open', TicketsORM.status == 'in_work')))
            )
            res = await session.execute(query)
            result = res.scalars().one_or_none()
            if result is None:
                return None
            else:
                return result

    @staticmethod
    async def all_tickets():
        async with async_session_factory() as session:
            query = select(TicketsORM)
            result = await session.execute(query)
            tickets = result.scalars().all()
            result_dto = [TicketDTO.model_validate(row, from_attributes=True) for row in tickets]
            return result_dto

    @staticmethod
    async def new_status_ticket(ticket_id, new_status):
        async with async_session_factory() as session:
            ticket = await session.get(TicketsORM, int(ticket_id))
            ticket.status = new_status
            session.add(ticket)
            await session.commit()
            await session.refresh(ticket)
            result_dto = TicketDTO.model_validate(ticket, from_attributes=True)
            return result_dto

    @staticmethod
    async def answer(ticket_id, answer):
        async with async_session_factory() as session:
            ticket = await session.get(TicketsORM, int(ticket_id))
            ticket.answer = answer
            session.add(ticket)
            await session.commit()
            await session.refresh(ticket)
            result_dto = TicketDTO.model_validate(ticket, from_attributes=True)
            return result_dto

    @staticmethod
    async def sort_by_created_time():
        async with async_session_factory() as session:
            query = select(TicketsORM).order_by(TicketsORM.created_at.desc())
            result = await session.execute(query)
            tickets = result.scalars().all()
            result_dto = [TicketDTO.model_validate(row, from_attributes=True) for row in tickets]
            return result_dto

    @staticmethod
    async def sort_by_updated_time():
        async with async_session_factory() as session:
            query = select(TicketsORM).order_by(TicketsORM.updated_at.desc())
            result = await session.execute(query)
            tickets = result.scalars().all()
            result_dto = [TicketDTO.model_validate(row, from_attributes=True) for row in tickets]
            return result_dto

    @staticmethod
    async def sort_by_status():
        async with async_session_factory() as session:
            query = select(TicketsORM).order_by(
                case(
                    {
                        'open': 1,
                        'in_work': 2,
                        'close': 3
                    },
                    value=TicketsORM.status
                )
            )
            result = await session.execute(query)
            tickets = result.scalars().all()
            result_dto = [TicketDTO.model_validate(row, from_attributes=True) for row in tickets]
            return result_dto
