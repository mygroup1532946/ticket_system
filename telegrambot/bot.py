from aiogram import Bot, Dispatcher, types
from config import BOT_TOKEN
import asyncio
import logging
import requests


bot = Bot(token=BOT_TOKEN)
dp = Dispatcher()

route_check_status = 'http://app:8000/status/ticket'


@dp.message()
async def create_ticket(message: types.Message):
    data = {
        'chat_id': message.chat.id,
        'text': message.text
    }
    res = requests.post(route_check_status, json=data)
    if res.json()[1] == 200:
        await bot.send_message(
            chat_id=message.chat.id,
            text=f'Тикет создан #{res.json()[0]}, ожидайте ответа',
            reply_to_message_id=message.message_id,
        )
    elif res.json()[1] == 400:
        await bot.send_message(
            chat_id=message.chat.id,
            text='Дождитесь ответа от оператора!',
            reply_to_message_id=message.message_id,
        )


async def main():
    logging.basicConfig(level=logging.INFO)
    await dp.start_polling(bot)


if __name__ == "__main__":
    asyncio.run(main())
